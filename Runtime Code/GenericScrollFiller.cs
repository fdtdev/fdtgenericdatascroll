using System.Collections;
using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;
using System;

namespace com.FDT.UI
{
    public class GenericScrollFiller<TItem, T2, TMgr> : MonoBehaviour where TMgr:GenericDataScrollBase<TItem, T2, TMgr>, IGenericScroll<TItem> where T2:GenericScrollItem<TItem, TMgr> where TItem : class
    {
        public List<TItem> itemsToFill = new List<TItem>();
        public TMgr scroll;

        public void Fill()
        {
            FillAsync();
        }
        public ScrollAsyncOp FillAsync()
        {
            ScrollAsyncOp r = new ScrollAsyncOp();
            StartCoroutine(DoFill(r));
            return r;
        }
        public virtual IEnumerator DoFill(ScrollAsyncOp callback)
        {
            yield return scroll.FillWithListAsync(itemsToFill);
            callback.IsDone = true;
            
        }
        public virtual void ClearItems()
        {
            scroll.ClearItems();
        }
        public void Refresh(List<TItem> refillItems)
        {
            RefreshAsync(refillItems);
        }
        public ScrollAsyncOp RefreshAsync(List<TItem> refillItems)
        {
            ScrollAsyncOp r = new ScrollAsyncOp();
            StartCoroutine(DoRefresh(refillItems));
            return r;
        }
        
        protected virtual IEnumerator DoRefresh(List<TItem> refillItems)
        {
            ClearItems();
            itemsToFill = refillItems;
            scroll.ContentSize.enabled = true;
            scroll.Group.enabled = true;
            yield return null;
            yield return FillAsync();
        }
    }
    
    public class ScrollAsyncOp : CustomYieldInstruction
    {
        public Action OnComplete;
        protected bool _isDone = false;

        public bool IsDone
        {
            get { return _isDone; }
            set { _isDone = value;
                if (_isDone && OnComplete != null)
                {
                    OnComplete();
                }
            }
        }

        public override bool keepWaiting
        {
            get { return !IsDone; }
        }
    }
    
}
