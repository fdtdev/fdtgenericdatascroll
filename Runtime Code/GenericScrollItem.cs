﻿using UnityEngine;
using Object = System.Object;

namespace com.FDT.UI
{
    public class GenericScrollItem<TItem, TMgr> : MonoBehaviour where TMgr:IGenericScroll<TItem> where TItem : class
    {
        #region Classes

        #endregion

        #region GameEvents

        //[Header("GameEvents"), SerializeField] 

        #endregion

        #region Actions

        #endregion

        #region UnityEvents

        //[Header("UnityEvents"), SerializeField] 

        #endregion

        #region Fields

        [Header("Fields"), SerializeField] protected TItem _item;
        [SerializeField] protected TMgr _mgr;
        #endregion

        #region Properties

        public TItem Item
        {
            get { return _item; }
        }

        #endregion

        #region Methods
        
        public virtual void Init(TItem item, TMgr mgr)
        {
            _item = item;
            _mgr = mgr;
        }

        public virtual void Pressed()
        {
            _mgr.Pressed(_item as TItem);
        }
        #endregion
    }
}