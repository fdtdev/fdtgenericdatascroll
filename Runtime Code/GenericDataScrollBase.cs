using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

namespace com.FDT.UI
{
    //#pragma warning disable 0693
    public class GenericDataScrollBase<TItem, T2, TMgr> : MonoBehaviour where T2:GenericScrollItem<TItem, TMgr> where TMgr : IGenericScroll<TItem> where TItem : class
    {
        #region Classes
        #endregion

        #region GameEvents

        //[Header("GameEvents"), SerializeField] 

        #endregion

        #region Actions

        #endregion

        #region UnityEvents

        [Header("UnityEvents"), SerializeField]
        protected UnityEvent _onFinishFillEvt;

        public UnityEvent OnFinishFillEvt
        {
            get { return _onFinishFillEvt; }
        }

        #endregion

        #region Fields

        [Header("References"), SerializeField, RedNull] protected RectTransform _container;
        [SerializeField, RedNull] protected T2 _listItemPrefab = null;

        [SerializeField, RedNull] protected ScrollRect _scrollRect;
        public ScrollRect ScrollRect
        {
            get { return _scrollRect; }
        }

        [SerializeField, RedNull] protected HorizontalOrVerticalLayoutGroup _layoutGroup;
        [SerializeField, RedNull] protected ContentSizeFitter _contentSizeFitter;
        
        [Header("Runtime"), SerializeField] protected List<TItem> _items = new List<TItem>();
        [SerializeField] protected List<T2> _listItems = new List<T2>();
        [SerializeField] protected List<T2> _listItemsToRemove = new List<T2>();
        [SerializeField] protected List<TItem> _itemsToRemove = new List<TItem>();

        [Header("Settings"), com.FDT.Common.HelpBox("Settings cannot be changed in runtime.", HelpBoxType.Info), SerializeField] protected bool _infinite;
        
        protected UI_InfiniteScroll _infiniteScroll;
        protected Transform dummyContainer;
        [SerializeField] protected long _budget = 12;

        [SerializeField] protected int _minItemsToScroll = 3;
        [SerializeField] protected bool _autoFixPosition = false;
        
        [SerializeField] protected float _num = 1000.0f;
        #endregion

        #region Properties
        public List<T2> ListItems
        {
            get { return _listItems; }
        }
        public ContentSizeFitter ContentSize
        {
            get { return _contentSizeFitter; }
            set { _contentSizeFitter = value; }
        }
        public HorizontalOrVerticalLayoutGroup Group
        {
            get => _layoutGroup;
            set => _layoutGroup = value;
        }
        public bool EnableInfinite
        {
            get => _infinite;
            set => _infinite = value;
        }
        
        #endregion

        #region Methods
        
        public virtual void Pressed(TItem item)
        {
            
        }
        
        protected bool filling = false;
        
        protected virtual IEnumerator DoFill(List<TItem> newItems, ScrollAsyncOp callback = null)
        {

            var sw = Stopwatch.StartNew();
            
            filling = true;
            
            List<T2> newInstances = new List<T2>();
            
            if (dummyContainer == null)
            {
                dummyContainer = new GameObject("dummyContainer").transform;
                dummyContainer.SetParent(_container.parent);
                dummyContainer.gameObject.SetActive(false);
            }
            
            sw.Start();
            for (int i = 0; i < newItems.Count; i++)
            {
                T2 p = CreateItem(newItems[i], dummyContainer);
                newInstances.Add(p);

               if (sw.ElapsedMilliseconds > _budget)
               {
                   yield return null;
                   sw.Reset();
                   sw.Start();
               }
            }
            
            if (_items.Count > 0)
            {
                _listItemsToRemove.FillWith(_listItems);
                _itemsToRemove.FillWith(_items);
                ClearItems();
                _items.Clear();
                _listItems.Clear();
            }
            
            if (_infiniteScroll != null)
                Destroy(_infiniteScroll);
            
            _layoutGroup.enabled = true;
            
            _items = newItems.ToList();
            _listItems = newInstances;
            
            for (int i = 0; i < _listItems.Count; i++)
            {
                _listItems[i].transform.SetParent(_container, false);
            }
            
            yield return null;

            _scrollRect.enabled = true;
            if (_infinite)
            {
                _layoutGroup.enabled = false;
                _contentSizeFitter.enabled = false;
                _infiniteScroll = gameObject.AddComponent<UI_InfiniteScroll>();
            }
            else
            {
                _contentSizeFitter.enabled = true;
                _layoutGroup.enabled = true;
            }
            filling = false;
            _scrollRect.horizontalNormalizedPosition = 1.0f;
            _scrollRect.verticalNormalizedPosition = 1.0f;
            sw.Stop();

            if (_listItems.Count <= _minItemsToScroll)
            {
                _scrollRect.enabled = false;
                _layoutGroup.enabled = false;
            }
            _onFinishFillEvt.Invoke();

            if (callback != null)
            {
                callback.IsDone = true;
            }
        }

        public virtual void ClearItems()
        {
            _itemsToRemove.Clear();
            for (int i = 0; i < _listItemsToRemove.Count; i++)
            {
                DestroyItem(i);
            }
            _listItemsToRemove.Clear();
        }

        public ScrollAsyncOp FillWithListAsync(List<TItem> newItems)
        {
            ScrollAsyncOp o = new ScrollAsyncOp();
            if (filling)
            {
                o.IsDone = true;
                UnityEngine.Debug.LogError("Called fill while filling");
                return o;
            }

            StartCoroutine(DoFill(newItems, o));
            return o;
        }
        
        public void FillWithList(List<TItem> newItems)
        {
            FillWithListAsync(newItems);
        }

        protected virtual void FixedUpdate()
        {
            if (_autoFixPosition)
                TestFix();
        }

        protected void TestFix()
        {
            if (!filling)
            {
                // TODO: horizontal
                if (_scrollRect.vertical)
                {
                    if (_container.transform.localPosition.y < -_num || _container.transform.localPosition.y > _num)
                    {
                        float offset = 0;
                        if (_container.transform.localPosition.y < -_num)
                        {
                            offset = _num;
                        }
                        else
                        {
                            offset = -_num;
                        }

                        _container.transform.localPosition = new Vector3(_container.transform.localPosition.x, _container.transform.localPosition.y+offset, _container.transform.localPosition.z);
                        for (int i = 0; i < _listItems.Count; i++)
                        {
                            _listItems[i].transform.localPosition = new Vector3(_listItems[i].transform.localPosition.x,
                                _listItems[i].transform.localPosition.y - offset, _listItems[i].transform.localPosition.z);
                        }
                    }
                }
                else if (_scrollRect.horizontal)
                {
                    if (_container.transform.localPosition.x < -_num || _container.transform.localPosition.x > _num)
                    {
                        float offset = 0;
                        if (_container.transform.localPosition.x < -_num)
                        {
                            offset = _num;
                        }
                        else
                        {
                            offset = -_num;
                        }

                        _container.transform.localPosition = new Vector3(_container.transform.localPosition.x+offset, _container.transform.localPosition.y, _container.transform.localPosition.z);
                        for (int i = 0; i < _listItems.Count; i++)
                        {
                            _listItems[i].transform.localPosition = new Vector3(_listItems[i].transform.localPosition.x - offset,
                                _listItems[i].transform.localPosition.y, _listItems[i].transform.localPosition.z);
                        }
                    }
                }
            }
        }
        protected virtual T2 CreateItem(TItem p, Transform parent)
        {
            T2 result = Instantiate<T2>(_listItemPrefab, parent);
            result.Init(p, (TMgr)(System.Object)this);
            return result;
        }
        public virtual void DestroyItem(int idx)
        {
            Destroy(_listItemsToRemove[idx].gameObject);
        }
        
        public bool GetNormalizedPosForItem(TItem item, out float n)
        {
            n = -1;
            T2 target = default(T2);
            bool found = false;
            foreach (var i in _listItems)
            {
                if (i.Item == item)
                {
                    target = i;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                n = _scrollRect.GetScrollCenterToTransform(target.transform as RectTransform);
                return true;
            }
            return false;
        }
        public void SetTarget(TItem item, float f)
        {
            Canvas.ForceUpdateCanvases();
            T2 target = default;
            bool found = false;
            foreach (var i in _listItems)
            {
                if (i.Item == item)
                {
                    target = i;
                    found = true;
                    break;
                }
            }

            if (found)
            {
                float n = _scrollRect.GetScrollCenterToTransform(target.transform as RectTransform);
                _scrollRect.SetScrollNormal(n);
            }
        }

        public void SetInfinite(bool v)
        {
            if (v)
            {
                TestFix();
            }
            if (_infiniteScroll!= null)
                _infiniteScroll.enabled = v;
        }
        #endregion
    }
    //#pragma warning restore 0693
}