﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.FDT.UI;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace com.FDT.UI
{
    [RequireComponent(typeof(CanvasGroup))]
public class GenericScrollFocusHandlerBase<TItem, T2, TMgr> : MonoBehaviour where TItem : class where T2 : GenericScrollItem<TItem, TMgr> where TMgr : IGenericScroll<TItem>
{
    [SerializeField] protected TItem _target;

    public TItem Target
    {
        get { return _target; }
    }

    [SerializeField] protected float _time;
    public float Time
    {
        get { return _time; }
    }

    [SerializeField] [NotNull] protected TMgr _scroll;

    [SerializeField] protected float _normalTarget;
    [SerializeField] protected bool _tweening = false;
    [SerializeField] protected float maxVel = 3f;
    
    private void OnEnable()
    {
        if (_scroll.ScrollRect.horizontal)
            _normalTarget = _scroll.ScrollRect.horizontalNormalizedPosition;
        else
            _normalTarget = _scroll.ScrollRect.verticalNormalizedPosition;
    }

    public void SetTarget(TItem t)
    {
        float n = -1;
        if (_scroll.GetNormalizedPosForItem(t, out n))
        {
            _target = t;
            _normalTarget = n;
            _tweening = true;
        }
    }

    protected float currentVel;
    [SerializeField] protected float smoothTime = 1f;
    [SerializeField] protected float minDist = 0.1f;
    [SerializeField] protected CanvasGroup _canvasGroup;

    private void Reset()
    {
        _canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Update()
    {
        if (_scroll.HasContent && _target != null)
        {
            if (_tweening)
            {
                if (_canvasGroup.blocksRaycasts)
                {
                    _canvasGroup.blocksRaycasts = false;
                }

                if (_scroll.InfiniteEnabled)
                    _scroll.SetInfinite(false);
                float cTarget;
                if (_scroll.GetNormalizedPosForItem(_target, out cTarget))
                {
                    _normalTarget = cTarget;
                }

                float currentNormal = 0;
                if (_scroll.ScrollRect.horizontal)
                    currentNormal = _scroll.ScrollRect.horizontalNormalizedPosition;
                else
                    currentNormal = _scroll.ScrollRect.verticalNormalizedPosition;

                float d = Mathf.Abs(Mathf.Abs(_normalTarget) - Mathf.Abs(currentNormal));

                if (d > minDist)
                {
                    float n = Mathf.SmoothDamp(currentNormal, _normalTarget, ref currentVel, smoothTime,
                        maxVel * UnityEngine.Time.deltaTime);
                    if (_scroll.ScrollRect.horizontal)
                        _scroll.ScrollRect.horizontalNormalizedPosition = n;
                    else
                        _scroll.ScrollRect.verticalNormalizedPosition = n;
                }
                else
                {
                    if (_scroll.ScrollRect.horizontal)
                        _scroll.ScrollRect.horizontalNormalizedPosition = _normalTarget;
                    else
                        _scroll.ScrollRect.verticalNormalizedPosition = _normalTarget;
                    _tweening = false;
                    _target = null;
                    _canvasGroup.blocksRaycasts = true;
                }
            }
            else
            {
                if (!_scroll.InfiniteEnabled)
                    _scroll.SetInfinite(false);
            }
        }
    }
}


}
