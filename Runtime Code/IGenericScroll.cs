﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace com.FDT.UI
{
    //#pragma warning disable 0693
    public interface IGenericScroll<TItem>
    {
        void Pressed(TItem item);
        void SetTarget(TItem item, float f);
        bool GetNormalizedPosForItem(TItem item, out float n);
        ScrollRect ScrollRect { get; }
        bool InfiniteEnabled { get; }
        void SetInfinite(bool v);

        bool HasContent { get; }

    }
    //#pragma warning restore 0693
}
